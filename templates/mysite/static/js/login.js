$(function () {
    var nav = $(".active");
    nav.attr("class", "tab-item");
    nav.next().next().attr("class", "tab-item active");
});
layui.use(['form', 'layedit', 'laydate'], function () {
    var form = layui.form
        , layer = layui.layer;


    //自定义验证规则
    form.verify({

    });


    //监听提交
    form.on('submit(demo1)', function (data) {
        var data = JSON.stringify(data.field);
        $.post("/checkLogin", $.parseJSON(data),
            function (data, status) {
                layer.msg(data);
                if (data == "登录成功") {
                    window.location.href = "refer";
                }
            });
        return false;
    });

});