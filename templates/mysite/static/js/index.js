$(function () {
    $(".status").html($(".myLi").attr("status"))

    // 获取评论，手风琴效果
    $('.comment').click(function () {
        var thisParent = $(this).parent();
        var fn = thisParent.parent().find(".comment_div");
        var fn_val = fn.css("display");
        var id = thisParent.attr("dataId");
        var myLi = $(".myLi");
        if (fn_val == "none") {
            $.get("/GetComment",
                {
                    msgId: id
                },
                function (data, status) {
                    fn.children('ul').html(" ");
                    for (var i = 0; i < data.length; i++) {
                        myLi.find(".nick").html(data[i]["nickname"]);
                        myLi.find(".time").html(data[i]["datetime"]);
                        myLi.find(".comment_text").html(data[i]["comment"]);
                        fn.children('ul').append($(".myLi").html());
                    }
                });
            fn.css("display", "block");
            // fn.children('ul').append($(".myLi").html());
        }
        else {
            fn.children('ul').html("");
            fn.css("display", "none");
        }

    });

    // 猜名字
    $(document).on('click', '.guess', function () {
        var id = $(this).parent().attr("dataId");
        $.prompt('猜猜我是谁！', function (value) {
            $.get("/cai", {
                id: id,
                cname: value
            }, function (data, status) {
                layer.msg(data);
            })
        });
    });

    //赞，向后台提交id，如果赞成功，自增1
    $(document).on('click', '.zan', function () {
        var id = $(this).parent().attr('dataId');
        myzan = $(this).children('sup');
        $.get("/zan", {
                id: id
            },
            function (data, status) {
                layer.msg(data);
                if (data == "赞+1") {
                    var zan = myzan.text();
                    zan = parseInt(zan) + 1;
                    myzan.text(zan);
                }
            });
    });

    $.init();
});

layui.use(['form', 'layedit', 'laydate'], function () {
    //加载layui模块
    var form = layui.form
        , layer = layui.layer;


    //自定义验证规则
    form.verify({

    });


    //监听提交
    form.on('submit(demo1)', function (data) {
        var thisParent = $(this).parents(".card");
        var fn = thisParent.find(".comment_div");
        var data = JSON.stringify(data.field);
        data = $.parseJSON(data);
        var id = thisParent.children(".getID").attr("dataId");
        data.msgId = id;
        $.post("/comment", data,
            function (data, status) {
                layer.msg(data);
                if (data == "评论成功") {

                }
            });
        return false;
    });


});