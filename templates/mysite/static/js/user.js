$(function () {
    var nav = $(".active");
    nav.attr("class", "tab-item");
    nav.next().next().attr("class", "tab-item active");
});

$(".userMsg").click(function () {
    window.location.replace("/usermsg");
});

$(".myList").click(function () {
    window.location.replace("/myList");
});

$(".quit").click(function () {
    $.get("quitLogin","",function (data) {
        layer.msg(data);
        if(data == "注销成功"){
            window.location.replace("/login");
        }
    });
});

layui.use(['form', 'layedit', 'laydate'], function () {
    var form = layui.form
        , layer = layui.layer;
});