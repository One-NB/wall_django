$(function () {
    var nav = $(".active");
    nav.attr("class", "tab-item");
    nav.next().next().attr("class", "tab-item active");
});
layui.use(['form', 'layedit', 'laydate'], function () {
    var form = layui.form
        , layer = layui.layer;


    //自定义验证规则
    form.verify({
        password: [/(.+){6,16}$/, '密码必须6到16位']
        , content: function (value) {
            layedit.sync(editIndex);
        }
    });


    //监听提交
    form.on('submit(demo1)', function (data) {
        // layer.alert(typeof json.param(JSON.stringify(data.field)), {
        //   title: '最终的提交信息'
        // })
        var data = JSON.stringify(data.field);
        data = $.parseJSON(data);
        if (data["password"] != data["r_password"]) {
            layer.msg("密码不一致");
        }
        else {
            $.post("/userData", data,
                function (data, status) {
                    layer.msg(data);
                    if (data == "注册成功") {
                        window.location.href = "login";
                    }
                });
        }
        return false;
    });


});