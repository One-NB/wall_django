$(function () {
    // alert("123")
    var nav = $(".active");
    nav.attr("class", "tab-item");
    nav.next().next().attr("class", "tab-item active");
});
layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form
  ,layer = layui.layer
  ,layedit = layui.layedit
  ,laydate = layui.laydate;


  //自定义验证规则
  form.verify({
    title: function(value){
      if(value.length < 5){
        return '标题至少得5个字符啊';
      }
    }
    ,pass: [/(.+){6,12}$/, '密码必须6到12位']
    ,content: function(value){
      layedit.sync(editIndex);
    }
  });



  //监听提交
  form.on('submit(demo1)', function(data){
    // layer.alert(typeof json.param(JSON.stringify(data.field)), {
    //   title: '最终的提交信息'
    // })
      var data = JSON.stringify(data.field);
      $.post("/userData",  $.parseJSON(data),
        function(data,status){
            layer.msg(data);
            if(data == "注册成功"){
                window.location.href="login";
            }
       });
    return false;
  });




});