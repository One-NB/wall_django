$(function () {
    var nav = $(".active");
    nav.attr("class", "tab-item");
    nav.next().attr("class", "tab-item active");
});
layui.use(['form', 'layedit', 'laydate'], function () {
    var form = layui.form
        , layer = layui.layer;


    //自定义验证规则
    form.verify({

    });


    //监听提交
    form.on('submit(demo1)', function (data) {
        var formData = new FormData();
        formData.append("img", $("#myimg")[0].files[0]);
        var data = JSON.stringify(data.field);
        formData.append("text", data);
        data = $.parseJSON(data);
        formData.append("csrfmiddlewaretoken", data["csrfmiddlewaretoken"]);

        $.ajax({
            type: "post",
            url: "referData",
            data: formData,
            async: false,
            contentType: false,
            processData: false,
            success: function (data) {
                layer.msg(data);
            },
            error: function (error) {
                layer.msg("提交失败");
            }
        });
        return false;
    });

});