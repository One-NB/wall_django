from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.hashers import make_password, check_password
from ..models import UserData


def checkUser(fn):
    def inner(request):
        if request.session.get('status') == 1:
            return fn(request)
        else:
            return HttpResponseRedirect('/login')
    return inner


def commentUser(fn):
    def inner(request):
        if request.session.get('status') == 1:
            return fn(request)
        else:
            return HttpResponse("请登录")
    return inner


def register(request):
    return render(request, 'register.html')


def login(request):
    return render(request, 'login.html')


def userData(request):
    try:
        data = request.POST
        data = data.dict()
        del data['csrfmiddlewaretoken'], data['r_password']
        data['password'] = make_password(data['password'], 'dsjgh12ED', 'pbkdf2_sha256')
        sql = UserData(**data)
        sql.save()
        return HttpResponse('注册成功')
    except BaseException:
        return HttpResponse('注册失败')


def checkLogin(request):
    try:
        data = request.POST
        password = UserData.objects.values("password").get(nickname=data['nickname'])
        status = check_password(data['password'], password['password'])
        if status:
            request.session['nickname'] = data['nickname']
            request.session['status'] = 1
            return HttpResponse('登录成功')
        else:
            return HttpResponse('密码或账号错误')
    except BaseException:
        return HttpResponse('登录失败')