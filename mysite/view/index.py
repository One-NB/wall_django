import json
import datetime

from django.core import serializers
from django.shortcuts import render
from .userLogin import commentUser

# Create your views here.
from django.http import HttpResponse
from ..models import UserData, UserMsg, reply


def json_default(value):
    if isinstance(value, datetime.date):
        mytime = str(value)
        return mytime
    else:
        return value.__dict__


def index(request):
    data = UserMsg.objects.all().order_by('-datetime')
    status = "未登录"
    if request.session.get('nickname'):
        status = "已登录"
    return render(request, 'index.html', context={'data': data, 'status': status})


@commentUser
def zan(request):
    id = request.GET['id']
    szan = request.session.get('zan')
    if szan:
        if id in szan:
            return HttpResponse("已赞")
        else:
            szan.append(id)
            request.session['zan'] = szan
            data = UserMsg.objects.get(id=int(id))
            data.zan += 1
            data.save()
            return HttpResponse("赞+1")
    else:
        request.session['zan'] = list(id)
        data = UserMsg.objects.get(id=int(id))
        data.zan += 1
        data.save()
        return HttpResponse("赞+1")


def GetComment(request):
    msgId = request.GET['msgId']
    data = reply.objects.values("nickname", "datetime", "comment").filter(msgId=int(msgId))
    return HttpResponse(json.dumps(list(data), default=json_default), content_type='application/json')


@commentUser
def comment(request):
    try:
        ip = request.META['REMOTE_ADDR']
        data = request.POST
        data = data.dict()
        del data['csrfmiddlewaretoken']
        data['ip'] = ip;
        nickname = request.session.get('nickname')
        data['nickname'] = nickname
        sql = reply(**data)
        sql.save()
        return HttpResponse("评论成功")
    except BaseException:
        return HttpResponse("评论失败")


@commentUser
def cai(request):
    data = request.GET
    id = int(data["id"])
    cname = data["cname"]
    nickname = UserMsg.objects.values("nickname").get(id=id)["nickname"]
    username = UserData.objects.values("username").get(nickname=nickname)["username"]
    if cname == username:
        return HttpResponse("被你猜对了")
    else:
        return HttpResponse("猜测错误")
