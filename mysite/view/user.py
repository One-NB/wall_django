from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .userLogin import checkUser
from ..models import UserData, UserMsg


@checkUser
def user(request):
    return render(request, 'user.html')


@checkUser
def userMsg(request):
    nickname = request.session.get('nickname')
    data = UserData.objects.get(nickname=nickname)
    return render(request, 'usermsg.html', context={"data": data})


# 我的表白
@checkUser
def myList(request):
    nickname = request.session.get('nickname')
    data = UserMsg.objects.filter(nickname=nickname)
    return render(request, 'index.html', context={"data": data, "status": "已登录"})


@checkUser
def quitLogin(request):
    try:
        request.session['status'] = 0
        return HttpResponse("注销成功")
    except BaseException:
        return HttpResponse("注销失败")
