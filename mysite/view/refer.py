from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .userLogin import checkUser
from ..models import UserData, UserMsg
import json
# from django.conf import settings


@checkUser
def refer(request):
    return render(request, 'refer.html')


@checkUser
def refer_data(request):
    ip = request.META['REMOTE_ADDR']
    img = request.FILES.get('img')
    print(type(img))
    data = request.POST["text"]
    data = json.loads(data)
    del data['csrfmiddlewaretoken'], data['img']
    nickname = request.session.get('nickname')
    data['nickname'] = nickname
    data['img'] = img
    data['ip'] = ip
    print(data)
    sql = UserMsg(**data)
    sql.save()
    return HttpResponse("提交成功")
