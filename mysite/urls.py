from django.contrib import admin
from django.urls import path
from .view import index, refer, user, userLogin

urlpatterns = [
    path('', index.index),
    path('zan', index.zan),
    path('comment', index.comment),
    path('GetComment', index.GetComment),
    path('cai', index.cai),
    path('refer', refer.refer),
    path('referData', refer.refer_data),
    path('user', user.user),
    path('usermsg', user.userMsg),
    path('quitLogin', user.quitLogin),
    path('myList', user.myList),
    path('register', userLogin.register),
    path('login', userLogin.login),
    path('userData', userLogin.userData),
    path('checkLogin', userLogin.checkLogin),
]
