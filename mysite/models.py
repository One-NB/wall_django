from django.db import models
from .system.storage import ImageStorage


class UserData(models.Model):
    nickname = models.CharField(max_length=20, unique=True, null=False, verbose_name='昵称')
    username = models.CharField(max_length=20, null=False, verbose_name='用户名')
    sex = models.CharField(max_length=10, null=False, verbose_name='性别')
    password = models.CharField(max_length=76, null=False)

    def __str__(self):  # 使用__str__帮助人性化显示对象信息
        return self.username

    class Meta:
        verbose_name = "用户"
        verbose_name_plural = "用户"


class UserMsg(models.Model):
    nickname = models.CharField(max_length=20, null=False, verbose_name='表白者昵称')
    hername = models.CharField(max_length=20, null=False, verbose_name='对方姓名')
    content = models.CharField(max_length=200, null=False)
    img = models.ImageField(upload_to='img/%Y/%m/%d', storage=ImageStorage())
    content = models.CharField(max_length=200, null=False, verbose_name='内容')
    zan = choose = models.IntegerField(default=0, verbose_name='赞')
    choose = models.IntegerField(default=0)
    ip = models.GenericIPAddressField()
    display = models.IntegerField(default=0)
    datetime = models.DateTimeField(auto_now_add=True, verbose_name='时间')

    def __str__(self):  # 使用__str__帮助人性化显示对象信息
        return self.nickname

    class Meta:
        verbose_name = "表白信息"
        verbose_name_plural = "表白信息"

    def show_content(self):
        if len(str(self.content)) > 20:
            return '{}......'.format(str(self.content)[0:20])
        else:
            return str(self.content)

    show_content.short_description = '内容'


class reply(models.Model):
    msgId = models.IntegerField(null=False, verbose_name='问题ID')
    nickname = models.CharField(max_length=20, null=False, verbose_name='评论者昵称')
    comment = models.TextField(max_length=100, verbose_name='评论内容')
    ip = models.GenericIPAddressField()
    datetime = models.DateTimeField(auto_now_add=True, verbose_name='时间')

    def __str__(self):  # 使用__str__帮助人性化显示对象信息
        return self.nickname

    def show_comment(self):
        if len(str(self.comment)) > 20:
            return '{}......'.format(str(self.comment)[0:20])
        else:
            return str(self.comment)

    show_comment.short_description = '内容'

    class Meta:
        verbose_name = "评论信息"
        verbose_name_plural = "评论信息"