from django.contrib import admin
from .models import *

@admin.register(UserData)
class UserDataAdmin(admin.ModelAdmin):
    list_display = ('username', 'nickname', 'sex')
    search_fields = ('username', 'nickname')  # 搜索字段


@admin.register(UserMsg)
class UserMsgAdmin(admin.ModelAdmin):
    # show_content = UserMsg.show_content()
    list_display = ('nickname', 'hername', 'show_content', 'zan', 'datetime')
    search_fields = ('hername', 'nickname')  # 搜索字段


@admin.register(reply)
class replyAdmin(admin.ModelAdmin):
    list_display = ('msgId', 'nickname', 'show_comment', 'datetime')
    search_fields = ('nickname', 'comment')  # 搜索字段